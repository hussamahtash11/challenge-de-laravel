<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  Route::get('/', function () {
//      return view('welcome');
//  });

 Route::get('/', function () {
     return view('home');
 });
// Route::view("home".'home');
//  Route::view("save".'save');
 
// Route::get('/green', function () {
//     return view('save');
// });
// Route::get('/black', function () {
//     return view('save');
// });
// Route::get('/gray', function () {
//     return view('save');
// });
// Route::get('/red', function () {
//     return view('save');
// });
Route::get('/save/{color}',[BlogController::class, "index"]);

Route::get('/save/{color}',[BlogController::class, "NewColor"]);


// Route::get('/save', function () {
//       return view('save');
//   });

